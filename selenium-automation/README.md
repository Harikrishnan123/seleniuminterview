# Automation UI testing framework with Selenium

Example skeleton for quick start with Selenium WebDriver and TestNG.

## Dependencies
- Java 8
- Maven
- Selenium WebDriver
- TestNG
- WebDriverManager

## logger & reporter
- log4j2 core
- Extent spark reporter 4

#######################################################################

