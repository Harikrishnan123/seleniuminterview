package pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utilities.Browser;


public class LandingPage extends BasePage {
    private static Logger logger = LogManager.getLogger(LandingPage.class);

    private final By pillowTitle = By.xpath("//*[@class='title']");
    private final By pillowCurrency = By.xpath("//*[@class='price']/span[1]");
    private final By btnSignUpClick = By.xpath("//*[@class='btn btn-signup']");
    private final By btnBuyNow = By.xpath("//a[@class='btn buy']");
    private final By Shoppingcart = By.xpath("//*[contains(text(),'Shopping Cart')]");
    private final By product = By.xpath("//*[text()='Product']");
    private final By qty = By.xpath("//*[text()='Qty']");
    private final By amountRp = By.xpath("//*[text()='Amount (Rp)']");
    private final By midtransPillow = By.xpath("//td[text()='Midtrans Pillow']");
    private final By total = By.xpath("//td[text()='Total']");
    private final By actualTotal = By.xpath("//td[text()='Total']/following-sibling::td");
    private final By cusDetails = By.xpath("//*[text()='Customer Details']");
    private final By cusDetailsName = By.xpath("//*[text()='Name']");
    private final By cusDetailsNameInput = By.xpath("//td[text()='Name']/following-sibling::td/input");
    private final By cusDetailsEmail = By.xpath("//*[text()='Email']");
    private final By cusDetailsEmailInput = By.xpath("//td[text()='Email']/following-sibling::td/input");
    private final By cusDetailsPhNo = By.xpath("//*[text()='Phone no']");
    private final By cusDetailsPhNoInput = By.xpath("//td[text()='Phone no']/following-sibling::td/input");
    private final By cusDetailsCity = By.xpath("//*[text()='City']");
    private final By cusDetailsCityInput = By.xpath("//td[text()='City']/following-sibling::td/input");
    private final By cusDetailsAddress = By.xpath("//*[text()='Address']");
    private final By cusDetailsAddressInput = By.xpath("//td[text()='Address']/following-sibling::td/textarea");
    private final By cusDetailsPostalCode = By.xpath("//*[text()='Postal Code']");
    private final By cusDetailsPostalCodeInput = By.xpath("//td[text()='Postal Code']/following-sibling::td/input");
    private final By cusDetailsCheckOut = By.className("cart-checkout");
    private final By cusDetailsCancelButton = By.className("cancel-btn");
    private final By checkoutOrdersummary = By.xpath("//*[@class=\"text-page-title\"]");
    private final By checkoutOrdersummary_amount = By.xpath("//*[@class='text-amount-title']");
    private final By checkoutOrdersummary_amount_currency = By.xpath("//*[@class='amount']//*[@class='text-amount-rp']");
    private final By checkoutOrdersummary_amount_currency_real = By.xpath("//*[@class='amount']//*[@class='text-amount-amount']");
    private final By checkoutOrdersummary_orderID = By.xpath("//*[@class='order-id-optional']/strong");
    private final By checkoutOrdersummary_get_orderID = By.xpath("//*[@class='order-id-optional']/div");
    private final By checkoutOrderDetails_items = By.xpath("//*[text()='item(s)']");
    private final By checkoutOrderDetails_items_amount = By.xpath("//th[text()='amount']");
    private final By checkoutOrderDetails_items_Details = By.xpath("//*[@class='item-name']");
    private final By checkoutOrderDetails_items_Details_amount = By.xpath("//*[@class='table-amount text-body']");
    private final By checkoutOrderDetails_items_Details_countinue_btn = By.xpath("//a[@href=\"#/select-payment\"]");
    private final By checkoutOrderDetails_shippingdetails_tab = By.xpath("//*[text()='shipping details']");
    private final By checkoutOrderDetails_shippingdetails_Name = By.xpath("//div[text()='Name']");
    private final By checkoutOrderDetails_shippingdetails_Name_Input = By.xpath("//div[text()='Name']/following-sibling::div");
    private final By checkoutOrderDetails_shippingdetails_Address = By.xpath("//div[text()='Address']");
    private final By checkoutOrderDetails_shippingdetails_Address_Input = By.xpath("//div[text()='Address']/following-sibling::div");
    private final By checkoutOrderDetails_shippingdetails_PhNo = By.xpath("//div[text()='Phone number']");
    private final By checkoutOrderDetails_shippingdetails_PhNo_Input = By.xpath("//div[text()='Phone number']/following-sibling::div");
    private final By checkoutOrderDetails_shippingdetails_Email = By.xpath("//div[text()='Email']");
    private final By checkoutOrderDetails_shippingdetails_Email_Input = By.xpath("//div[text()='Email']/following-sibling::div");
    private final By logostore = By.xpath("//*[@class=\"logo-store\"]");
    private final By selectPayment = By.xpath("//*[text()='Select Payment']");
    private final By btnSignUp = By.xpath("//*[@class='btn btn-signup']");
    private final By creditcardNumber = By.name("cardnumber");
    private final By creditcardMonthYear = By.xpath("//*[@placeholder='MM / YY']");
    private final By creditcardMonthCVV = By.xpath("//*[@placeholder='123']");
    private final By creditcardpayNow = By.xpath("//*[@href='#/']");
    private final By creditcardpayNowInvalidCard = By.xpath("//*[text()='Invalid card number']");

    public void validateProduct() {
        String pillowTitleProduct = getPillowTitle();
        Assert.assertEquals(pillowTitleProduct, "Midtrans Pillow");
        Assert.assertEquals(getPillowCurrency(), "Rp");
        Assert.assertEquals(getPillowCurrencyPrice(), "20,000");
        Assert.assertEquals(getPillowDes(), "Get cozy with our new pillow!");
        Assert.assertEquals(getPillowDesFirst(), "Disclaimer: This is not a real transaction.No funds will be transfered out of your account. You should sleep with a proper pillow to refrain yourself from injury.");
        Assert.assertEquals(getBuyButton(), "BUY NOW");
        WebDriverWait wait = new WebDriverWait(Browser.getDriver(), 5);
        Assert.assertEquals(getReadyIntegrateSnap(), "Ready to integrate SNAP?");
        Assert.assertEquals(btnSignUp(), "SIGN UP  →");
        logger.info("validated the fields"+ pillowTitleProduct);
        logger.info("validated the fields"+ getPillowCurrency());
        logger.info("validated the fields"+ getPillowCurrencyPrice());
        logger.info("validated the fields"+ getPillowDes());
        logger.info("validated the fields"+ getPillowDesFirst());
        logger.info("validated the fields"+ getBuyButton());
        logger.info("validated the fields"+ getReadyIntegrateSnap());
        logger.info("validated the fields"+ btnSignUp());
    }

    public void assertShoppingCart() {
        Assert.assertEquals(getBuyButton(), "BUY NOW");
        logger.info("validated the fields"+ getBuyButton());
        waitAndClick(btnBuyNow);
        logger.info("clicked on the Buy button");
        logger.info("validated the fields"+ waitAndGetText(Shoppingcart).trim());
    }

    public void customerDetails()
    {
        Assert.assertEquals(waitAndGetText(Shoppingcart).trim(), "Shopping Cart");
        Assert.assertEquals(waitAndGetText(product), "Product");
        Assert.assertEquals(waitAndGetText(qty), "Qty");
        Assert.assertEquals(waitAndGetText(amountRp), "Amount (Rp)");
        Assert.assertEquals(waitAndGetText(midtransPillow), "Midtrans Pillow");
        Assert.assertEquals(waitAndGetText(total), "Total");
        Assert.assertEquals(waitAndGetText(actualTotal), "20,000");
        Assert.assertEquals(waitAndGetText(cusDetails), "Customer Details");
        Assert.assertEquals(waitAndGetText(cusDetailsName), "Name");
        Assert.assertEquals(waitAndGetText(cusDetailsEmail), "Email");
        Assert.assertEquals(waitAndGetText(cusDetailsPhNo), "Phone no");
        Assert.assertEquals(waitAndGetText(cusDetailsCity), "City");
        Assert.assertEquals(waitAndGetText(cusDetailsAddress), "Address");
        Assert.assertEquals(waitAndGetText(cusDetailsPostalCode), "Postal Code");
        Assert.assertEquals(waitAndGetText(cusDetailsCheckOut), "CHECKOUT");
        logger.info("validated the fields Shopping Cart");
        logger.info("validated the fields Product");
        logger.info("validated the fields Qty");
        logger.info("validated the fields Amount (Rp)");
        logger.info("validated the fields Midtrans Pillow");
        logger.info("validated the fields Total");
        logger.info("validated the fields Customer Details, Name, Email, Phone No, city, address, postalcode, checkout button");

    }
    public void orderSummary(String cardSelection, String cardNo, String cardMonth, String cvv)
    {
        int size = Browser.getDriver().findElements(By.tagName("iframe")).size();
        {
            for(int i=0; i<=size-1; i++) {
                Browser.getDriver().switchTo().frame(i);

                logger.info("Switch to frame");
                if(Browser.getDriver().findElement(By.xpath("//*[@class=\"text-page-title\"]")).isDisplayed()) {
                    waitForElement(checkoutOrdersummary);
                    Assert.assertEquals(waitAndGetText(checkoutOrdersummary), "Order Summary");
                    Assert.assertEquals(waitAndGetText(checkoutOrdersummary_amount), "amount");
                    Assert.assertEquals(waitAndGetText(checkoutOrdersummary_amount_currency), "Rp");
                    Assert.assertEquals(waitAndGetText(checkoutOrdersummary_amount_currency_real), "20,000");
                    Assert.assertEquals(waitAndGetText(checkoutOrdersummary_orderID), "Order ID");
                    logger.info("Order ID is " + waitAndGetText(checkoutOrdersummary_get_orderID));
                    Assert.assertEquals(waitAndGetText(checkoutOrderDetails_items), "item(s)");
                    Assert.assertEquals(waitAndGetText(checkoutOrderDetails_items_amount), "amount");
                    Assert.assertEquals(waitAndGetText(checkoutOrderDetails_items_Details_amount), "20,000");
                    Assert.assertEquals(waitAndGetText(checkoutOrderDetails_items_Details), "Midtrans Pillow");
                    waitAndClick(checkoutOrderDetails_items_Details_countinue_btn);
                    paymentList();
                }
                paymentListCreditcard(cardSelection, cardNo, cardMonth, cvv);
                Browser.getDriver().switchTo().defaultContent();

            }
        }
    }
    public void paymentList()
    {
        int sizePaymentDetails = Browser.getDriver().findElements(By.xpath("//*[@class='list-content']")).size();
        for(int i=1;i<=sizePaymentDetails;i++)
        {
            String getListDetailsTitle = Browser.getDriver().findElement(By.xpath("(//*[@class='list-content'])["+i+"]/div[1]")).getText();
            String getListDetailsName = Browser.getDriver().findElement(By.xpath("(//*[@class='list-content'])["+i+"]/div[2]")).getText();
            logger.info("Payment details :  "+getListDetailsTitle + " details "+getListDetailsName);
        }
    }


    public void paymentListCreditcard(String cardSelection, String cardNo, String cardMonth, String cvv)
    {
        int sizePaymentDetails = Browser.getDriver().findElements(By.xpath("//*[@class='list-content']")).size();
        for(int i=1;i<=sizePaymentDetails;i++)
        {
            String getListDetailsTitle = Browser.getDriver().findElement(By.xpath("(//*[@class='list-content'])["+i+"]/div[1]")).getText();
            if(getListDetailsTitle.equals(cardSelection)) {
                Browser.getDriver().findElement(By.xpath("(//*[@class='list-content'])["+i+"]/div[1]")).click();
                waitAndEnterText(creditcardNumber,cardNo);
                waitAndEnterText(creditcardMonthYear,cardMonth);
                waitAndEnterText(creditcardMonthCVV,cvv);

                if(Browser.getDriver().findElements(By.xpath("//*[text()='Invalid card number']")).size()==0) {
                    waitAndClick(creditcardpayNow);
                }
                else
                {
                    logger.info("Invalid card details");
                }

                logger.info("clicked on the details no "+creditcardNumber +"card month"+cardMonth+" card cvv"+cvv);
                break;
            }
            break;

        }
    }


    public void customerInputDetails(String cusName, String cusEmail, String phonno,String city, String address, String postalCode)
    {
        waitAndEnterText(cusDetailsNameInput,cusName);
        waitAndEnterText(cusDetailsEmailInput,cusEmail);
        waitAndEnterText(cusDetailsPhNoInput,phonno);
        waitAndEnterText(cusDetailsCityInput,city);
        waitAndEnterText(cusDetailsAddressInput,address);
        waitAndEnterText(cusDetailsPostalCodeInput,postalCode);
        waitAndClick(cusDetailsCheckOut);
        logger.info("Entered name:"+cusName+"\n cusEmail:"+cusEmail+"\n phonno:"+phonno+"city "+city+" address"+address +"postalCode and clicked on checkoutbutton" );
    }

    public void goBuyNow() { waitAndClick(btnSignUpClick);
    }

}

