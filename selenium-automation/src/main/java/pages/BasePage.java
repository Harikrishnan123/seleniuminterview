package pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.Browser;

public class BasePage {

    private final By title = By.className("title");
    private final By currency = By.xpath("//*[@class='price']/span[1]");
    private final By currencyPrice = By.xpath("//*[@class='price']/span[2]");
    private final By pillowDes = By.xpath("(//*[@class='desc'])[1]/span");
    private final By pillowDes1 = By.xpath("(//*[@class='desc'])[1]/i");
    private final By buyButton = By.xpath("//a[@class='btn buy']");
    private final By readyIntegrateSnap = By.xpath("//*[@class='desc-cta']");
    private final By btnSignUp = By.xpath("//*[@class='btn btn-signup']");

    public String getPillowTitle() {
        return Browser.getDriver().findElement(title).getText();
    }

    public String getPillowCurrency() {
        return Browser.getDriver().findElement(currency).getText();
    }

    public String getPillowCurrencyPrice() {
        return Browser.getDriver().findElement(currencyPrice).getText();
    }

    public String getPillowDes() {
        return Browser.getDriver().findElement(pillowDes).getText();
    }

    public String getPillowDesFirst() {
        return Browser.getDriver().findElement(pillowDes1).getText();
    }

    public String getBuyButton() {
        return Browser.getDriver().findElement(buyButton).getText();
    }

    public String getReadyIntegrateSnap() {
        waitForElement(readyIntegrateSnap);
        return Browser.getDriver().findElement(readyIntegrateSnap).getText();
    }

    public String btnSignUp() {
        return Browser.getDriver().findElement(btnSignUp).getText();
    }

    public void waitAndClick(By element) {
        WebDriverWait wait = new WebDriverWait(Browser.getDriver(), 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        retryingFindClick(element);
    }
    public void waitForElement(By element) {
        WebDriverWait wait = new WebDriverWait(Browser.getDriver(), 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }
    public void checkAlert() {
        try {
            WebDriverWait wait = new WebDriverWait(Browser.getDriver(), 2);
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = Browser.getDriver().switchTo().alert();
            alert.accept();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //to avoid stale element exception
    public boolean retryingFindClick(By by) {
        boolean result = false;
        int attempts = 0;
        while(attempts < 2) {
            try {
                Browser.getDriver().findElement(by).click();
                result = true;
                break;
            } catch(Exception e) {
            }
            attempts++;
        }
        return result;
    }

    public void waitAndEnterText(By element, String text) {
        WebDriverWait wait = new WebDriverWait(Browser.getDriver(), 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element)).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(element)).sendKeys(text);
    }

    public String getFormattedString(String locator, String value) {
        return String.format(locator, value);
    }

    public void waitForSeconds(int time) {
        try {
            Thread.sleep(time * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String waitAndGetText(By element) {
        WebDriverWait wait = new WebDriverWait(Browser.getDriver(), 5);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(element)).getText();
    }

}
