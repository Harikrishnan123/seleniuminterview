package tests;

import org.apache.logging.log4j.LogManager;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LandingPage;
import utilities.Browser;
import org.apache.logging.log4j.Logger;

public class LandingPageTest extends TestBase{
    private static Logger logger = LogManager.getLogger(LandingPageTest.class);
    private LandingPage landingPage = new LandingPage();

    @Test (priority=1)
    public void testPageTitle() {
        System.out.println(Browser.getTitle());
        Assert.assertEquals(Browser.getTitle(), "Sample Store");
        logger.info("Title verified!");
    }

    @Test (priority=2)
    public void navigateCategories() {

        landingPage.getPillowTitle();
    }

    @Test (priority=3)
    public void validateProductDetailsPillow() {
        landingPage.validateProduct();
    }

    @Test (priority=4)
    public void validateShoppingCart() {
        landingPage.assertShoppingCart();
        landingPage.customerDetails();

    }

    @Test (priority=5)
    public void validateByEnteringCustomerDetailsOrderWithSucessFlow() {
        landingPage.customerInputDetails("Hari", "cusEmailHari001@mailinator.com","1232343434","singapore","Raffelspplace","798987");
        landingPage.orderSummary("Credit/Debit Card", "4811 1111 1111 1114", "02/20", "112233");

    }

    @Test (priority=6)
    public void validateByEnteringCustomerDetailsOrderWithFailureFlow() {
        landingPage.orderSummary("Credit/Debit Card", "4811 1111 1111 1113", "02/20", "112233");

    }
}
